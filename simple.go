package main

import (
	"errors"
	"fmt"
	"strconv"
	"time"
	"encoding/json"

	"github.com/hyperledger/fabric/core/chaincode/shim"
)

// チェーンコードが実装される構造体
type SimpleChaincode struct {
}

//  履歴データの構造体
type History struct {
	Transactions []Transaction `json:"transactions"`
}

//取引の構造体
type Transaction struct {
	Balance     int    `json:"balance"`
	Fluctuation int    `json:"fluctuation"`
	Date        string `json:"date"`
	Opponent    string `json:"opponent"`
	Description string `json:"description"`
}

//初期化関数
func (t *SimpleChaincode) Init(stub *shim.ChaincodeStub, function string, args []string) ([]byte, error) {

	var user string   //コインを持っているユーザ
	var coin int      //ユーザが持っているコイン
	var err error

	//引数の数が正しいかチェック
	if len(args) % 2 != 0 {
		return nil, errors.New("Incorrect number of arguments. Expecting even number")
	}

	//引数の値が正しいかチェック
	for i := 0; i < len(args); i += 2 {
		user = args[i]
		coin, err = strconv.Atoi(args[i + 1])
		if ( err != nil) {
			return nil, errors.New("Expecting integer value for coin.")
		}
		fmt.Printf("user = %s, coin = %d\n", user, coin)
	}

	//全ての引数の値が正しい場合、ワールドステートに各ユーザの初期値を書き込む
	for i := 0; i < len(args); i += 2 {
		//コイン残高をワールドステートに持つ
		user = args[i]
		coin, err = strconv.Atoi(args[i + 1])
		err = stub.PutState(user, []byte(strconv.Itoa(coin)))
		if err != nil {
			return nil, err
		}

		////コイン履歴をワールドステートに持つ
		history := History{}
		history.Transactions = []Transaction{}
		jsonAsBytes, _ := json.Marshal(history)
		err = stub.PutState(user + "_history", jsonAsBytes)
		if err != nil {
			return nil, err
		}
	}

	return nil, nil
}

func (t *SimpleChaincode) Invoke(stub *shim.ChaincodeStub, function string, args []string) ([]byte, error) {
	if function == "remit" {
		// 送金メソッド
		return t.remit(stub, args)
	}

	return nil, errors.New("Invalid invoke function name.")
}

// 送金メソッド
func (t *SimpleChaincode) remit(stub *shim.ChaincodeStub, args []string) ([]byte, error) {
	var remitter, remittee string        // 送金者と受金者
	var remitterCoin, remitteeCoin int   // 送金者と受金者のコイン残高
	var remittance int                   // 送金額
	var history History                  // 取引履歴
	var err error

	//引数の数をチェック
	if len(args) != 4 {
		return nil, errors.New("Incorrect number of arguments. Expecting 3")
	}

	//引数の値をチェック
	remittance, err = strconv.Atoi(args[2])
	if ( err != nil) {
		return nil, errors.New("Expecting integer value for remittance.")
	}

	//ワールドステートからそれぞれの残コインを取得
	remitter = args[0]
	remittee = args[1]

	remitterCoinBytes, err := stub.GetState(remitter)
	if err != nil {
		return nil, errors.New("Failed to get state")
	}
	if remitterCoinBytes == nil {
		return nil, errors.New("Entity not found")
	}
	remitterCoin, _ = strconv.Atoi(string(remitterCoinBytes))

	remitteeCoinBytes, err := stub.GetState(remittee)
	if err != nil {
		return nil, errors.New("Failed to get state")
	}
	if remitteeCoinBytes == nil {
		return nil, errors.New("Entity not found")
	}
	remitteeCoin, _ = strconv.Atoi(string(remitteeCoinBytes))

	// 送金額の値を増減させる
	remitterCoin = remitterCoin - remittance
	remitteeCoin = remitteeCoin + remittance
	if (remitterCoin < 0) {
		return nil, errors.New("Insufficient balance of remitter's coin")
	}
	fmt.Printf("remitterCoin = %d, remitteeCoin = %d\n", remitterCoin, remitteeCoin)

	// ワールドステートに送金後残高を書き込む
	err = stub.PutState(remitter, []byte(strconv.Itoa(remitterCoin)))
	if err != nil {
		return nil, err
	}

	err = stub.PutState(remittee, []byte(strconv.Itoa(remitteeCoin)))
	if err != nil {
		return nil, err
	}

	//　ワールドステートに取引の履歴を書き込む
	//送金者の履歴の書き込み
	historyAsBytes, err := stub.GetState(remitter + "_history")
	if err != nil {
		return nil, errors.New("Failed to get opentrades")
	}
	json.Unmarshal(historyAsBytes, &history)
	history.Transactions = append(history.Transactions, Transaction{Balance:remitterCoin, Fluctuation:remittance * -1, Date:time.Now().Format("2006/01/02 15:04:05"),Opponent:remittee,Description:args[3]})
	jsonAsBytes, _ := json.Marshal(history)
	err = stub.PutState(remitter + "_history", jsonAsBytes)
	if err != nil {
		return nil, err
	}

	//受金者の履歴の書き込み
	historyAsBytes, err = stub.GetState(remittee + "_history")
	if err != nil {
		return nil, errors.New("Failed to get opentrades")
	}
	json.Unmarshal(historyAsBytes, &history)
	history.Transactions = append(history.Transactions, Transaction{Balance:remitteeCoin, Fluctuation:remittance, Date:time.Now().Format("2006/01/02 15:04:05"),Opponent:remitter,Description:args[3]})
	jsonAsBytes, _ = json.Marshal(history)
	err = stub.PutState(remittee + "_history", jsonAsBytes)
	if err != nil {
		return nil, err
	}

	return nil, nil
}

func (t *SimpleChaincode) Query(stub *shim.ChaincodeStub, function string, args []string) ([]byte, error) {
	if function == "getBalance" {
		return t.getBalance(stub, args)
	}
	if function == "getHistory" {
		return t.getHistory(stub, args)
	}

	return nil, errors.New("Invalid query function name.")
}

// 残高照会メソッド
func (t *SimpleChaincode) getBalance(stub *shim.ChaincodeStub, args []string) ([]byte, error) {
	var user string // ユーザー名
	var err error

	//引数の数をチェック
	if len(args) != 1 {
		return nil, errors.New("Incorrect number of arguments. Expecting name of the person to get balance")
	}

	// ワールドステートからユーザの残高を取得する
	user = args[0]
	userCoinBytes, err := stub.GetState(user)
	if err != nil {
		jsonResp := "{\"Error\":\"Failed to get state for " + user + "\"}"
		return nil, errors.New(jsonResp)
	}
	if userCoinBytes == nil {
		jsonResp := "{\"Error\":\"Nil amount for " + user + "\"}"
		return nil, errors.New(jsonResp)
	}

	jsonResp := "{\"Name\":\"" + user + "\",\"Amount\":\"" + string(userCoinBytes) + "\"}"
	fmt.Printf("Query Response:%s\n", jsonResp)
	return userCoinBytes, nil
}

// 履歴照会メソッド
func (t *SimpleChaincode) getHistory(stub *shim.ChaincodeStub, args []string) ([]byte, error) {
	var user string // ユーザー名
	var err error

	//引数の数をチェック
	if len(args) != 1 {
		return nil, errors.New("Incorrect number of arguments. Expecting name of the person to get balance")
	}

	// ワールドステートからユーザの履歴を取得する
	user = args[0]
	userHistoryBytes, err := stub.GetState(user + "_history")
	if err != nil {
		jsonResp := "{\"Error\":\"Failed to get state for " + user + "\"}"
		return nil, errors.New(jsonResp)
	}
	if userHistoryBytes == nil {
		jsonResp := "{\"Error\":\"Nil history for " + user + "\"}"
		return nil, errors.New(jsonResp)
	}

	jsonResp := "{\"Name\":\"" + user + "\",\"History\":\"" + string(userHistoryBytes) + "\"}"
	fmt.Printf("Query Response:%s\n", jsonResp)
	return userHistoryBytes, nil
}

func main() {
	err := shim.Start(new(SimpleChaincode))
	if err != nil {
		fmt.Printf("Error starting Simple chaincode: %s", err)
	}
}